;; This is an operating system configuration template
;; for a "desktop" setup with GNOME and Xfce where the
;; root partition is encrypted with LUKS.
(use-modules
 (gnu)
 (gnu system nss)
 (orange system samus))

(define this-file
  (local-file (basename (assoc-ref (current-source-location) 'filename))
              "config.scm"))
(operating-system
  (inherit installation-os-samus))
