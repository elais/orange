(define-module (orange services lxqt)
  #:use-module (gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages lxqt)
  #:use-module (gnu services)
  #:use-module (gnu services desktop)
  #:use-module (gnu services base)
  #:use-module (gnu services dbus)
  #:use-module (guix deprecation)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (guix store)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:export (lxqt-desktop-configuration
            lxqt-desktop-configuration?
            lxqt-desktop-service
            lxqt-desktop-service-type))

(define (package-direct-input-selector input)
  (lambda (package)
    (match (assoc-ref (package-direct-inputs package) input)
      ((package . _) package))))

(define-record-type* <lxqt-desktop-configuration> lxqt-desktop-configuration
  make-lxqt-desktop-configuration
  lxqt-desktop-configuration?
  (lxqt-package lxqt-package (default lxqt)))

(define lxqt-desktop-service-type
  (service-type
   (name 'lxqt-desktop)
   (extensions
    (list (service-extension profile-service-type
                             (compose list
                                      lxqt-package))))
   (default-value (lxqt-desktop-configuration))
   (description "Run the LXQT desktop environment.")))

(define-deprecated (lxqt-desktop-service
                    #:key (config (lxqt-desktop-configuration)))
  lxqt-desktop-service-type
  "Return a service that adds the @code{lxqt} package to the system profile,
and extends polkit with the actions from @code{lxqt-policykit}."
  (service lxqt-desktop-service-type config))
