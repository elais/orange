;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2013, 2015 Andreas Enge <andreas@enge.fr>
;;; Copyright © 2013, 2014, 2015, 2016, 2017, 2018, 2019 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2014, 2015, 2016, 2017, 2018, 2019 Mark H Weaver <mhw@netris.org>
;;; Copyright © 2015 Sou Bunnbu <iyzsong@gmail.com>
;;; Copyright © 2016, 2017, 2018 Efraim Flashner <efraim@flashner.co.il>
;;; Copyright © 2016 Alex Griffin <a@ajgrf.com>
;;; Copyright © 2017 Clément Lassieur <clement@lassieur.org>
;;; Copyright © 2017 ng0 <ng0@n0.is>
;;; Copyright © 2017, 2018 Tobias Geerinckx-Rice <me@tobias.gr>
;;; Copyright © 2018 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2019 Ivan Petkov <ivanppetkov@gmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (orange packages midori)
  #:use-module (gnu)
  #:use-module ((srfi srfi-1) #:hide (zip))
  #:use-module (ice-9 match)
  #:use-module (gnu packages)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages video)
  #:use-module (gnu packages webkit)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix build utils)
  #:use-module (guix utils)
  #:use-module (rnrs io ports)
  #:use-module ((guix licenses) #:prefix license:))

(define-public midori
  (package
    (name "midori")
    (version "9.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/midori/core/archive/v"
                           version
                           ".tar.gz"))
       (sha256
        (base32
         "1nv0liz5d8ykd0vzqkkxzs8ajvsxfqg8cbdiyv2kvk6xjnx7qfli"))))
    (build-system glib-or-gtk-build-system)
    (arguments
     `(#:make-flags (list (string-append "prefix=" (assoc-ref %outputs "out")))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'check)
         (replace 'build
           (lambda _
             (mkdir "_build")
             (chdir "_build")
             (invoke "cmake"
                     (string-append
                      "-DCMAKE_INSTALL_PREFIX="
                      (assoc-ref %outputs "out"))
                     "-DCMAKE_BUILD_TYPE=RelWithDebInfo"
                     (string-append
                      "-DCMAKE_INSTALL_RPATH="
                      (assoc-ref %outputs "out") "/lib" ";"
                      (assoc-ref %outputs "out")
                      "/bin")
                     (string-append
                      "-DCMAKE_INSTALL_LIBDIR="
                      (assoc-ref %outputs "out") "/lib")
                     "..")
             (invoke "make" "-j" "4")
             #t)))))
    (native-inputs
     `(("intltool" ,intltool)
       ("cmake" ,cmake)
       ("ninja" ,ninja)
       ("vala" ,vala)
       ("pkg-config" ,pkg-config)))
    (inputs
     `(("libsoup" ,libsoup)
       ("sqlite" ,sqlite)
       ("glib" ,glib)
       ("gst-plugins-bad" ,gst-plugins-ugly)
       ("gst-plugins-good" ,gst-plugins-good)
       ("gst-libav" ,gst-libav)
       ("gcr" ,gcr)
       ("ffmpeg" ,ffmpeg)
       ("libpeas" ,libpeas)
       ("webkitgtk" ,webkitgtk)
       ("json-glib" ,json-glib)
       ("libarchive" ,libarchive)))
    (home-page "https://midori.org")
    (description "A lightweight, fast and free web browser using WebKit and GTK+")
    (synopsis "A lightweight, fast and free web browser using WebKit and GTK+")
    (license (list license:lgpl2.1+))))
midori
