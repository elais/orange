(define-module (orange packages linux)
  #:use-module (nongnu packages linux)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system linux-module)
  #:use-module (guix build-system trivial)
  #:use-module (guix licenses))

(define-public (linux/samus)
  (package
    (inherit linux-4.14)
    (name "linux")
    (native-inputs
     `(("kconfig" ,(local-file "pconfig"))
       ,@(alist-delete "kconfig"
                       (package-native-inputs linux-4.14))))
    (home-page "https://galliumos.com")
    (synopsis "The Linux kernel, with GalliumOS patches.")
    (description "The Linux kernel, with GalliumOS patches.")))
linux/samus
