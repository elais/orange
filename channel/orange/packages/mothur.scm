(define-module (orange packages mothur)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages readline)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build-system gnu))

(define-public mothur ; Probably works, but haven't really tested it. Probably
                      ; also requires packaging of uchime. This is bundled with
                      ; mothur but is available at
                      ; http://drive5.com/uchime/uchime_download.html.
  (package
    (name "mothur")
    (version "1.38.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://github.com/mothur/mothur/archive/v"
             version ".tar.gz"))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32
         "15xzwn6d9520xfwwlyy5vpp9r5p5xvkwjf219ki87c3k98a9xhp0"))))
    (build-system gnu-build-system)
    (arguments
     `(;; There are no tests.  See
       ;; https://github.com/mothur/mothur/issues/196
       #:tests? #f
       #:make-flags (list (string-append
                           "BOOST_LIBRARY_PATH=\""
                           (assoc-ref %build-inputs "boost")
                           "/lib\"")
                          (string-append
                           "BOOST_INCLUDE_DIR=\""
                           (assoc-ref %build-inputs "boost")
                           "/include\"")
                          (string-append
                           "MOTHUR_FILES=\\\""
                           (assoc-ref %outputs "out")
                           "/share/mothur\\\""))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (replace 'install
         (lambda* (#:key outputs #:allow-other-keys)
           (let ((bin (string-append (assoc-ref outputs "out") "/bin/")))
             (install-file "mothur" bin)))))))
    (inputs
     `(("boost" ,boost)
       ("readline" ,readline)
       ("zlib" ,zlib)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:gpl3)))
mothur
