(define-module (orange system samus)
  #:use-module (gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages fonts)
  #:use-module (gnu system)
  #:use-module (gnu system install)
  #:use-module (gnu system keyboard)
  #:use-module (gnu system shadow)
  #:use-module (gnu services)
  #:use-module (gnu services desktop)
  #:use-module (gnu services networking)
  #:use-module (gnu services ssh)
  #:use-module (gnu services sddm)
  #:use-module (gnu services xorg)
  #:use-module (nongnu packages linux)
  #:use-module (orange packages cmt)
  #:use-module (guix packages)
  #:use-module (srfi srfi-1))


(define %samus/xorg-modules
  (append (list xf86-input-cmt)
          (map specification->package
               '("xf86-video-intel" "xf86-input-evdev" "xf86-input-keyboard"))))

(define %elais/xorg-fonts
  (map specification->package
       '("font-ibm-plex" "font-gnu-unifont")))


(define %samus/keyboard-layout
  (keyboard-layout "us" "altgr-intl" #:model "chromebook"))

(define-public %orange/default-packages
  (append
   (map specification->package
        '("alsa-utils" "emacs" "git" "fish" "gash" "openbox" "compton"
          "ncurses" "nss-certs"))
   %base-packages))

(define-public %orange/samus-services
  (cons*
   (service sddm-service-type
            (sddm-configuration
             (xorg-configuration
              (xorg-configuration
               (modules %samus/xorg-modules)
               (fonts (append %elais/xorg-fonts %default-xorg-fonts))
               (keyboard-layout %samus/keyboard-layout)))))
   (service openssh-service-type)
   (modify-services
       (remove (lambda (service)
                 (member (service-kind service)
                         (list gdm-service-type)))
               %desktop-services))))
