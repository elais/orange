(specifications->manifest
 '(;; browsers
   "eolie"
   ;; forgive me stallman for I have sinned
   "nix"
   ;; fonts
   "font-ibm-plex"
   ;; GNOME
   "gnome-shell-extensions" "termite" "gnome-tweaks"
   ;; C
   "ccls"
   ;; lisp
   "sbcl"
   ;; tools
   "ghc-pandoc" "git" "direnv" "shellcheck" "aspell"))
